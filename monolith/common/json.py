from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet
from presentations.models import Status, Presentation
from events.models import Conference, Location


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            dictionary = {}
            if hasattr(o, "get_api_url"):
                dictionary["href"] = o.get_api_url()
            for prop in self.properties:
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop]
                    value = encoder.default(value)
                dictionary[prop] = value
            # dictionary.update(self.get_extra_data(o))
            return dictionary
        else:
            return super().default(o)

from common.json import ModelEncoder
from .models import Presentation, Status
from events.encoders import ConferenceListEncoder


class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "name",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "title",
        "status",
    ]
    encoders = {
        "status": StatusEncoder(),
    }


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

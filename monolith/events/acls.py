from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

# Uses the Pexels API to retrieve a picture based on a city and state
def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    # Make the request
    response = requests.get(
        url, headers=headers
    )  # kwarg because headers is lower on the list
    # Parse the JSON response
    picture_url = response.json()["photos"][0]["src"][
        "original"
    ]  # .json() is from the Requests module
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return {"picture_url": picture_url}


# Uses the Open Weather API to retrieve weather data from a specified city and state
def get_weather_data(city, state):
    # Use the Open Weather API
    query = f"{city},{state},US"
    params = {
        "q": query,
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Create the URL for the geocoding API with the city and state
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    geo_response = requests.get(geo_url, params=params)
    # Parse the JSON response
    geo_data = geo_response.json()
    print("GeoData----->", geo_data)
    # Get the latitude and longitude from the response
    # print("TEST:------------------->", geo_response.json())
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]
    # Create the URL for the current weather API with the latitude and longitude
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    request = requests.get(url)
    # Parse the JSON response
    response = request.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionarypy
    # Return the dictionary
    return {
        "temp": response["main"]["temp"],
        "description": response["weather"][0]["description"],
    }
